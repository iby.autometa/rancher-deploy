# Deploy Rancher cluster using GitLab CI and Terraform

## How to use

### Stages of the build
1. **Validate** - Will validate the configuration files in the working directory.
2. **Plan** - Will run terraform plan and create an execution plan.
3. **Apply** - Will run terraform apply and execute the plan. This is a manual step.
4. **Destroy** - Will destroy all resources created in the apply stage. This is a manual step as well.

### Apply
In order to execute `terraform apply` navigate to the `CI/CD` section of your project. Click on `New Pipeline`. And run a new pipeline.
Once the `validate` and `plan` stages have been completed. Click on the `apply` step and run.

### Destroy
To destroy the deployment click on the `destroy` step in the CD/CD console and run.

## Infrastructure Diagram
  <img align="center" width="100%" src="img/gitlab-rancher.png">

## Variables
In order to set the number of nodes for the cluster, Kubernetes version, Rancher version etc. You will need to navigate to the `Settings` page of the project and under `CI/CD` you can set yuor variables.

### Suggested values for environment variables:
| Var Name               | Var Value                | Var Type |
| :------:               |  :------:                | :------: |
| VPC                    | Your AWS VPC             | String   |
| AWS_ACCESS_KEY_ID      | Your AWS Access Key      | String   |
| AWS_SECRET_ACCESS_KEY  | Your AWS Secret Key      | String   |
| DOMAIN                 | Your domain name         | String   |
| EMAIL                  | Email of maintainer      | String   |
| INSTANCE_TYPE          | AWS EC2 Instance Type    | String   |
| K8S_VERSION            | Latest K8s version       | String   |
| NUMBER_OF_NODES        | 3                        | String   |
| RANCHER_VERSION        | Latest Rancher version   | String   |
| UI_PASSWORD            | Password for Rancehr UI  | String   |
| BUCKET_NAME            | Name of S3 bucket        | String   |
| BUCKET_KEY             | Path to tfstate file     | String   |

## GitLab Runner

When using this project make sure you have a gitlab runner registered. You can use this repo to set the runner up with the proper configuration: [gitlab-runner-aws](https://gitlab.com/iby.rancher/gitlab-runner-aws). Or follow the instruction in gitlab docs to setup a new runner: [GitLab Runner Installation](https://docs.gitlab.com/runner/install/).

If you already have a runner active. You can simply add this configuration:

```
#Register the runner
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "<Your project token>" \
  --executor "docker" \
  --docker-image hashicorp/terraform \
  --description "docker-runner" \
  --tag-list "" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```
