resource "random_id" "id" {
  byte_length = 8
}

locals {
  name               = "rancher-ha-${random_id.id.hex}"
  rancher_version    = var.rancher_version
  kubernetes_version = var.kubernetes_version
  le_email           = var.email
  domain             = var.domain
  instance_type      = var.instance_type
  master_node_count  = var.number_of_nodes
}

variable "rancher_password" {
  type        = string
}

/*variable "github_client_id" {
  type        = string
  default     = ""
  description = "GitHub client ID for Rancher to use, if using GH auth"
}
variable "github_client_secret" {
  type        = string
  default     = ""
  description = "GitHub client secret for Rancher to use, if using GH auth"
}*/
